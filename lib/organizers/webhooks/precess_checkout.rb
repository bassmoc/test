class Organizers::Webhooks::PrecessCheckout
  include Interactor::Organizer

  organize Interactors::Webhooks::Verify,
           Interactors::Webhooks::VerifyStore,
           Interactors::Webhooks::PrepareCheckout,
           Interactors::Webhooks::SaveCheckout
end

