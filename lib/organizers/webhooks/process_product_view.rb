class Organizers::Webhooks::ProcessProductView
  include Interactor::Organizer

  organize Interactors::Webhooks::VerifyStore,
           Interactors::Webhooks::SaveProductViews
end
