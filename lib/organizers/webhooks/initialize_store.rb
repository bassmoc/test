class Organizers::Webhooks::InitializeStore
  include Interactor::Organizer

  organize Interactors::Webhooks::StartSession,
           Interactors::Webhooks::AddWebhooks,
           Interactors::Webhooks::AddAssets,
           Interactors::Webhooks::RenderAssets
end

