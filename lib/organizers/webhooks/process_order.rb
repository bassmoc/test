class Organizers::Webhooks::ProcessOrder
  include Interactor::Organizer

  organize Interactors::Webhooks::Verify,
           Interactors::Webhooks::VerifyStore,
           Interactors::Webhooks::PrepareOrder,
           Interactors::Webhooks::SaveOrder,
           Interactors::Webhooks::SaveOrderStatistics,
           Interactors::Webhooks::SaveConversionRate
end

