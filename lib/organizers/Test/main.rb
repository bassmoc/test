class Organizers::Test::Main
  include Interactor::Organizer

  organize Interactors::Test::Sum, Interactors::Test::Mult, Interactors::Test::Div
end

