class Interactors::Test::Sum
  include Interactor

  before do
    context.sum = 0
  end

  def call
    context.sum = context.x + context.y
  end
end

