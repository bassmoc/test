class Interactors::Test::Mult
  include Interactor

  before do
    context.mult = 0
  end

  def call
    context.mult = context.x * context.y
  end
end