class Interactors::Test::Div
  include Interactor

  before do
    context.result = 0
  end

  def call
    if context.sum.nil? || context.sum == 0
      context.fail!(errors: "division by zero")
    else
      context.result = context.mult / context.sum
    end
  end
end