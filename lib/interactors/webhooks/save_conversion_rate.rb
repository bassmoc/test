class Interactors::Webhooks::SaveConversionRate
  include Interactor
  include Repositories::OrderStatistic

  def call
    st_params = {shop_id: context.shop.id, year: Date.today.year, yday: Date.today.yday}
    conversion_rate = ConversionRatePerDay.find_or_create_by(st_params)

    statistics = ShopStatistic.find_by(st_params)
    products_sold_count = statistics.data['products_sold']
    unique_views_count = statistics.data['unique_views']

    result = conversion_rate.update(
      orders_quantity: products_sold_count,
      views_quantity: unique_views_count,
      conversion_rate: (products_sold_count.to_f / unique_views_count.to_f * 100)
    )

    context.fail!(message: 'conversion_rate not created') unless result

    Rails.logger.info 'conversion_rate saved'
  end
end
