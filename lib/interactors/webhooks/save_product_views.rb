class Interactors::Webhooks::SaveProductViews
  include Interactor
  include Repositories::ViewsStatistic

  def call
    views_key = "#{context.product_id}_#{context.user_token}"
    st_params = {shop_id: context.shop.id, year: Date.today.year, yday: Date.today.yday}
    statistics = ShopStatistic.find_or_create_by(st_params)
    not_unique_view = statistics.data['view_keys'].include? views_key

    unless not_unique_view
      unique_views_saved = Repositories::ViewsStatistic.increment_unique_views(st_params, views_key)
      context.fail!(message: 'unique views not incremented') unless unique_views_saved
    end

    total_views_saved = Repositories::ViewsStatistic.increment_total_views(st_params)
    context.fail!(message: 'total_views not incremented') unless total_views_saved

    Rails.logger.info 'statistics saved'
  end
end
