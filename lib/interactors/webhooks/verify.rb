class Interactors::Webhooks::Verify
  include Interactor

  def call
    calculated_hmac = Base64.strict_encode64(OpenSSL::HMAC.digest('sha256', ENV['SHOPIFY_API_SECRET'], context.data))
    context.fail!(message: 'webhook not verified') unless ActiveSupport::SecurityUtils.secure_compare(calculated_hmac, context.hmac_header)
  end
end

