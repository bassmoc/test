class Interactors::Webhooks::SaveOrderStatistics
  include Interactor
  include Repositories::OrderStatistic

  def call
    st_params = {shop_id: context.shop.id, year: Date.today.year, yday: Date.today.yday}
    statistics = ShopStatistic.find_or_create_by(st_params)
    context.fail!(message: 'statistics not created') unless statistics

    products_sold_saved = Repositories::OrderStatistic.increment_products_sold(st_params, context.qty)
    context.fail!(message: 'products_sold quantity not incremented') unless products_sold_saved

    Rails.logger.info 'statistics saved'
  end
end
