class Interactors::Webhooks::SaveOrder
  include Interactor

  def call
    order_created = context.shop.orders.create(
      order_id: context.order_id,
      token: context.token,
      qty: context.qty
    )
    context.fail!(message: 'order not saved') unless order_created
    Rails.logger.info 'order saved'
  end
end
