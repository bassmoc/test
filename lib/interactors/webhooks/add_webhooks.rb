class Interactors::Webhooks::AddWebhooks
  include Interactor

  def call
    Rails.logger.info 'webhooks'
    context.webhooks.each do |webhook|
      context.fail!(message: 'session not created') unless ShopifyAPI::Webhook.new(webhook)
    end
    context.all_webhooks = ShopifyAPI::Webhook.all
  end
end
