class Interactors::Webhooks::VerifyStore
  include Interactor

  def call
    context.shop = Shop.find_by(shopify_domain: context.store_domain)
    context.fail!(message: 'store not found') unless context.shop
  end
end

