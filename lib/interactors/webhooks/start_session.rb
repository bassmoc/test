class Interactors::Webhooks::StartSession
  include Interactor

  def call
    context.store = Shop.find_by(shopify_domain: context.domain)
    context.fail!(message: 'store not found') unless context.store
    context.session = ShopifyAPI::Session.new(domain: context.domain, token: context.store.shopify_token, api_version: "2021-01")
    ShopifyAPI::Base.activate_session(context.session)
    context.fail!(message: 'session not created') unless context.session
  end
end

