class Interactors::Webhooks::AddAssets
  include Interactor

  def call
    Rails.logger.info 'assets'
    theme = ShopifyAPI::Theme.first
    context.assets.each do |asset|
      snippet = ShopifyAPI::Asset.new(key: asset[:key], theme_id: "#{theme.id}")
      snippet.attach(File.read(File.join(Rails.root, asset[:path])))
      snippet.save
      context.fail!(message: "snippet #{asset.key} not created") unless snippet
    end
  end
end
