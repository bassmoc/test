class Interactors::Webhooks::SaveCheckout
  include Interactor

  def call
    # save using hash
    created_checkout = context.shop.checkouts.create(
      checkout_id: context.checkout_id,
      token: context.token,
      cart_token: context.cart_token,
      qty: context.qty
    )

    context.fail!(message: 'checkout not saved') unless created_checkout
    Rails.logger.info 'checkout saved'
  end
end
