class Interactors::Webhooks::RenderAssets
  include Interactor

  def call
    Rails.logger.info 'render'
    product_page = ShopifyAPI::Asset.find(context.template)
    unless product_page.value.match(/#{context.render}/i)
      product_page.value.sub!(/<\/script>/, "</script>#{context.render}")
    end
    context.fail!(message: 'render error') unless product_page.save
    ShopifyAPI::Base.clear_session
  end
end
