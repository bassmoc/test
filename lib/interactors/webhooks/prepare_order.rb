class Interactors::Webhooks::PrepareOrder
  include Interactor

  def call

    data = JSON.parse context.data
    context.order_id = data['id']
    context.token = data['token']
    context.qty = data['line_items'].sum { |value| value['quantity'] }
  end
end
