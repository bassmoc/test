class Interactors::Webhooks::PrepareCheckout
  include Interactor

  def call
    context.checkout_id = context.data['id']
    context.token = context.data['token']
    context.cart_token = context.data['cart_token']
    context.qty = context.data['line_items'].sum { |value| value['quantity'] }
  end
end
