$( document ).ready(function() {
    actOnUser = function(retry, func){
        if (ShopifyAnalytics && ShopifyAnalytics.lib && ShopifyAnalytics.lib.user) {
            return ShopifyAnalytics.lib.user().traits().uniqToken;
        } else {
            if (retry > 0) {
                setTimeout(function() {
                    func(retry - 1, func);
                }, 1000);
            }
            console.log('User not ready'); // Can be removed - just for debug
        }
    }
    setTimeout(function() {
        var user_token = actOnUser(10, actOnUser);
        $.get('https://'+domain+'/apps/upsell-checkout/api/v1/product_page_views', { product_id: product_id, user_token: user_token })
    }, 1000);
});
