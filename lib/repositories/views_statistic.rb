module Repositories
  module ViewsStatistic
    def self.create(views_key)
      {
        products_sold: 0,
        view_keys: [views_key],
        unique_views: 1,
        total_views: 01
      }
    end

    def self.increment_unique_views(st_params, views_key)
      Rails.logger.info st_params
      increment_total_views = <<-SQL
      UPDATE "shop_statistics" SET
      data = data || ('{\"unique_views\": ' || ((data->>'unique_views')::int + 1) || '}')::jsonb
      WHERE "shop_statistics"."shop_id" = #{st_params[:shop_id]} AND "shop_statistics"."year" = #{st_params[:year]} AND "shop_statistics"."yday" = #{st_params[:yday]}
      SQL
      incremented_total_views = ActiveRecord::Base.connection.execute(increment_total_views)

      add_views_key = <<-SQL
      UPDATE "shop_statistics" SET
      data = jsonb_set(data::jsonb, array['view_keys'], (data->'view_keys')::jsonb || '[\"#{views_key}\"]'::jsonb)
      WHERE "shop_statistics"."shop_id" = #{st_params[:shop_id]} AND "shop_statistics"."year" = #{st_params[:year]} AND "shop_statistics"."yday" = #{st_params[:yday]}
      SQL
      added_views_key = ActiveRecord::Base.connection.execute(add_views_key)

      incremented_total_views && added_views_key
    end

    def self.increment_total_views(st_params)
      increment_total_views = <<-SQL
      UPDATE "shop_statistics" SET
      data = data || ('{\"total_views\": ' || ((data->>'total_views')::int + 1) || '}')::jsonb
      WHERE "shop_statistics"."shop_id" = #{st_params[:shop_id]} AND "shop_statistics"."year" = #{st_params[:year]} AND "shop_statistics"."yday" = #{st_params[:yday]}
      SQL
      ActiveRecord::Base.connection.execute(increment_total_views)
    end
  end
end

#UPDATE "shop_statistics" SET data = data || ('{"unique_views": ' || ((data->>'unique_views')::int + 1) || '}')::jsonb WHERE "shop_statistics"."shop_id" = $1 AND "shop_statistics"."year" = $2 AND "shop_statistics"."yday" = $3 [["shop_id", 1], ["year", 2021], ["yday", 85]]

#UPDATE "shop_statistics" SET data = jsonb_set(data::jsonb, array['view_keys'], (data->'view_keys')::jsonb || '["6566203490494_8dfda97d-3ce1-4aeb-9d8a-98012ec1c224"]'::jsonb) WHERE "shop_statistics"."shop_id" = $1 AND "shop_statistics"."year" = $2 AND "shop_statistics"."yday" = $3 [["shop_id", 1], ["year", 2021], ["yday", 85]]

#UPDATE "shop_statistics" SET data = data || ('{"total_views": ' || ((data->>'total_views')::int + 1) || '}')::jsonb WHERE "shop_statistics"."shop_id" = $1 AND "shop_statistics"."year" = $2 AND "shop_statistics"."yday" = $3 [["shop_id", 1], ["year", 2021], ["yday", 85]]
