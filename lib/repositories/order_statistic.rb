module Repositories
  module OrderStatistic
    def self.create(qty)
      {
        products_sold: qty,
        view_keys: [],
        unique_views: 0,
        total_views: 0
      }
    end

    def self.increment_products_sold(st_params, qty)
      increment_products_sold = <<-SQL
      UPDATE "shop_statistics" SET
      data = data || ('{\"products_sold\": ' || ((data->>'products_sold')::int + #{qty}) || '}')::jsonb
      WHERE "shop_statistics"."shop_id" = #{st_params[:shop_id]} AND "shop_statistics"."year" = #{st_params[:year]} AND "shop_statistics"."yday" = #{st_params[:yday]}
      SQL
      ActiveRecord::Base.connection.execute(increment_products_sold)
    end
  end
end
