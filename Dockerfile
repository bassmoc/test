FROM ruby:2.7.0
RUN apt-get update -qq && apt-get install -y nodejs
WORKDIR /myapp
COPY Gemfile /myapp/Gemfile
COPY Gemfile.lock /myapp/Gemfile.lock
RUN bundle install
COPY . /myapp



# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]