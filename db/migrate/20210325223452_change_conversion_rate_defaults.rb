class ChangeConversionRateDefaults < ActiveRecord::Migration[5.2]
  def change
    change_table :conversion_rate_per_days do |t|
      t.change :conversion_rate, :decimal, precision: 6, scale: 2, default: '0' #WTF NoMethodError: undefined method `to_sym' for {:default=>"0"}:Hash when default: 0
      t.change :orders_quantity, :integer, default: 0 #WTF NoMethodError: undefined method `to_sym' for {:default=>"0"}:Hash when no datatype
      t.change :views_quantity, :integer, default: 0
    end
  end
end
