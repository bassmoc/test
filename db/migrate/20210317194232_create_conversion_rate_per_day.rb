class CreateConversionRatePerDay < ActiveRecord::Migration[5.2]
  def change
    create_table :conversion_rate_per_days do |t|
      t.integer :shop_id, null: false
      t.integer :orders_quantity, null: false
      t.integer :views_quantity, null: false
      t.integer :conversion_rate, null: false
      t.integer :year, null: false
      t.integer :yday, null: false
    end
  end
end
