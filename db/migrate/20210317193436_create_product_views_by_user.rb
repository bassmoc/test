class CreateProductViewsByUser < ActiveRecord::Migration[5.2]
  def change
    create_table :product_views_by_users do |t|
      t.string :product_id, null: false
      t.string :user_token, null: false
      t.integer :shop_id, null: false
      t.integer :quantity, null: false
      t.integer :year, null: false
      t.integer :yday, null: false
    end
  end
end
