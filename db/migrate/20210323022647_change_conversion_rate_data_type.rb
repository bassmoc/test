class ChangeConversionRateDataType < ActiveRecord::Migration[5.2]
  def change
    change_table :conversion_rate_per_days do |t|
      t.change :conversion_rate, :decimal, precision: 6, scale: 2
    end
  end
end
