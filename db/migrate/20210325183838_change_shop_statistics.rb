class ChangeShopStatistics < ActiveRecord::Migration[5.2]
  def change
    change_table :shop_statistics do |t|
      t.change :data, :jsonb, default: {products_sold: 0, view_keys: [], unique_views: 0, total_views: 0}
    end
  end
end
