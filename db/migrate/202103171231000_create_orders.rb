class CreateOrders < ActiveRecord::Migration[5.2]
  def self.up
    create_table :orders do |t|
      t.string :order_id, null: false
      t.string :token, null: false
      t.integer :qty, null: false
      t.integer :shop_id, null: false
      t.timestamps
    end

    add_index :orders, :order_id, unique: true
  end

  def self.down
    drop_table :orders
  end
end
