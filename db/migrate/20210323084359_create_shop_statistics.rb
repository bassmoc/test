class CreateShopStatistics < ActiveRecord::Migration[5.2]
  def change
    create_table :shop_statistics do |t|
      t.integer :shop_id, null: false
      t.json :data, null: false
      t.integer :year, null: false
      t.integer :yday, null: false
      t.timestamps
    end
  end
end
