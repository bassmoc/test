class CreateOrderPerDay < ActiveRecord::Migration[5.2]
  def change
    create_table :order_per_days do |t|
      t.integer :shop_id, null: false
      t.integer :quantity, null: false
      t.integer :year, null: false
      t.integer :yday, null: false
    end
  end
end
