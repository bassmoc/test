class CreateCheckouts < ActiveRecord::Migration[5.2]
  def self.up
    create_table :checkouts do |t|
      t.string :checkout_id, null: false
      t.string :token, null: false
      t.string :cart_token, null: false
      t.integer :qty, null: false
      t.integer :shop_id, null: false
      t.timestamps
    end

    add_index :checkouts, :checkout_id, unique: true
  end

  def self.down
    drop_table :checkouts
  end
end
