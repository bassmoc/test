# frozen_string_literal: true

class HomeController < ApplicationController
  include ShopifyApp::EmbeddedApp
  include ShopifyApp::RequireKnownShop
  include ShopifyApp::ShopAccessScopesVerification
  PRODUCT_VIEW_LIQUID = 'lib/assets/product_views/product-view.liquid'.freeze
  PRODUCT_VIEW_JS = 'lib/assets/product_views/prod-view.js'.freeze

  def index
    @shop_origin = current_shopify_domain

    result = Organizers::Webhooks::InitializeStore.call(
      domain: @shop_origin,
      webhooks: [
        {
          topic: 'orders/create',
          address: "https://#{@shop_origin}/api/api/v1/orders_create",
          format: 'json'
        }
      ],
      assets: [
        { key: 'snippets/product-view.liquid', path: PRODUCT_VIEW_LIQUID },
        { key: 'assets/prod-view.js', path: PRODUCT_VIEW_JS }
      ],
      render: '{% render "product-view", domain: shop.domain, product_id: product.id, user_id: customer.id %}',
      template: 'templates/product.liquid'
    )
    Rails.logger.info result.message
    @webhooks = result.all_webhooks

    st_params = {shop_id: result.store.id, year: Date.today.year}
    @conversion_rates = ConversionRatePerDay.where(st_params)
  end
end
