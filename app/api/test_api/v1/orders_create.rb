module TestApi
  module V1
    class OrdersCreate < Grape::API

      resource :orders_create do
        desc 'orders/create webhook'
        post do
          request.body.rewind
          result = Organizers::Webhooks::ProcessOrder.call(data: request.body.read, hmac_header: headers["X-Shopify-Hmac-Sha256"], store_domain: headers["X-Shopify-Shop-Domain"])
          Rails.logger.info result.message
        end
      end
    end
  end
end
