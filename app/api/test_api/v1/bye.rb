module TestApi
  module V1
    class Bye < Grape::API
      include Repositories::OrderStatistic
      resource :bye do
        desc "read data"
        get do


          st_params = {shop_id: 1, year: Date.today.year, yday: 84}
          conversion_rate = ConversionRatePerDay.find_or_create_by(st_params)
          context.fail!(message: 'conversion_rate not created') unless conversion_rate

          statistics = ShopStatistic.find_by(st_params)
          products_sold_count = statistics.data['products_sold']
          unique_views_count = statistics.data['unique_views']

          conversion_rate.update(
            orders_quantity: products_sold_count,
            views_quantity: unique_views_count,
            conversion_rate: (products_sold_count.to_f / unique_views_count.to_f * 100)
          )
        end
      end
    end
  end
end
