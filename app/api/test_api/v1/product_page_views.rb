module TestApi
  module V1
    class ProductPageViews < Grape::API
      resource :product_page_views do
        get do
          product_id = params['product_id']
          user_token = params['user_token']
          store_domain = params['shop']

          result = Organizers::Webhooks::ProcessProductView.call(product_id: product_id, user_token: user_token, store_domain: store_domain)
          Rails.logger.info result.message
        end
      end
    end
  end
end
