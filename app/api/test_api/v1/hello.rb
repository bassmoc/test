module TestApi
  module V1
    class Hello < Grape::API

      helpers do
        # Compare the computed HMAC digest based on the shared secret and the request contents
        # to the reported HMAC in the headers
        def verify_webhook(data, hmac_header)
          calculated_hmac = Base64.strict_encode64(OpenSSL::HMAC.digest('sha256', "8cc5443e1cf1dbfc9b6185d9c5c25b64d912c494d5813c3d526c6f94410c2ec6", data))
          ActiveSupport::SecurityUtils.secure_compare(calculated_hmac, hmac_header)
        end
      end

      resource :hello do
        desc "testing routing"
        post "", root: :hello do
          #result = organizers::Test::Main.call(x: 143, y: -143)
          data = request.body
          verified = verify_webhook(data, env["HTTP_X_SHOPIFY_HMAC_SHA256"])
          save_path = Rails.root.join('tmp', 'temp.txt')
          begin
            file = File.open(save_path, "w")
            file.write({time: Time.now.strftime("%d/%m/%Y %H:%M"), head: request.headers, body: request.body, verified: verified})
            {path: File.absolute_path(save_path)}
          rescue IOError => e
            #some error occur, dir not writable etc.
          ensure
            file.close unless file.nil?
          end
          {head: request.headers, body: request.body}
        end
      end
    end
  end
end
