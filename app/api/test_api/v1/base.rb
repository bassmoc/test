module TestApi
  module V1
    class Base < Grape::API

      version :v1, using: :path

      prefix "api"

      default_format :json
      format :json

      mount TestApi::V1::Hello
      mount TestApi::V1::Bye
      mount TestApi::V1::CheckoutsCreate
      mount TestApi::V1::OrdersCreate
      mount TestApi::V1::ProductPageViews
    end
  end
end
