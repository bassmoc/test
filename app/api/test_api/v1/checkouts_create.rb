module TestApi
  module V1
    class CheckoutsCreate < Grape::API

      resource :checkouts_create do
        desc 'checkouts/create wenhook'
        post do
          request.body.rewind
          result = Organizers::Webhooks::PrecessCheckout.call(data: request.body.read, hmac_header: headers["X-Shopify-Hmac-Sha256"], store_domain: headers["X-Shopify-Shop-Domain"])
          Rails.logger.info result.message
        end
      end
    end
  end
end
