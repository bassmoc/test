# frozen_string_literal: true
class Shop < ActiveRecord::Base
  include ShopifyApp::ShopSessionStorageWithScopes
  has_many :checkouts
  has_many :orders
  has_many :product_views_by_users
  has_many :order_per_days
  has_many :conversion_rate_per_days
  has_many :shop_statistics

  def api_version
    ShopifyApp.configuration.api_version
  end
end
