#Rails.application.routes.draw do
#  root :to => 'home#index'
#  get '/products', :to => 'products#index'
#  mount ShopifyApp::Engine, at: '/'
#  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
#end

Rails.application.routes.draw do
  root :to => 'home#index'
  get '/products', :to => 'products#index'
  mount ShopifyApp::Engine, at: '/'
  mount TestApi::Base, at: "/api"
  mount TestApi::Checkouts, at: "/checkouts"

end