require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module MyShopifyAppB
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.0
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    config.action_dispatch.default_headers["P3P"] = 'CP="Not used"'
    config.action_dispatch.default_headers.delete("X-Frame-Options")
    config.paths.add File.join("app", "api"), glob: File.join("**", "*.rb")
    if Rails.env.development? || Rails.env.test?
      config.autoload_paths += %W[
        #{config.root}/lib
        #{config.root}/app/workers
        #{config.root}/app/api
        #{config.root}/app/interactors
      ]
      config.autoload_paths += Dir["#{config.root}/vendor/modules/**/"]
      config.autoload_paths += Dir[Rails.root.join("app", "api", "*")]
    else
      config.eager_load_paths += %W[
        #{config.root}/lib
        #{config.root}/app/workers
        #{config.root}/app/api
        #{config.root}/app/interactors
      ]
    end
    config.web_console.whitelisted_ips = '172.20.0.1'
  end
end
